**Jenkins Deploy**

**Playbook targets**

1.  Instance build in RHOSP
2.  Install & Configure Jenkins server

**Pre-requisites**

*  Update all required variable in `group_vars/openstack` file
*  Update your local system user's SSH key in `roles/instancebuild/tasks/main.yml` file
*  Update your Redhat Login in `roles/jenkins/vars/main.yml`
*  Update all required variables to customize instance in `roles/instancebuild/vars/main.yml `file

**Steps to Deploy**

`ansible-playbook -i hosts os-jenkins-provision.yml`

**Deployment Validation**

*  Access the Jenkins console via `http://<IP-ADDRESS>:8080/`
*  Update the Jenkins initial Admin password
*  Install the initial default plugins